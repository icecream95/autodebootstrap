# autodebootstrap

This is a script to make the process of getting a Debian chroot
downloaded easier.

It does not actually install Debian; it only runs the first stage of
debootstrap, which is architecture-independent.

# Usage:

You might want to edit the `install` script to change the release and
architecture of Debian - it defaults to Buster with the i386 (32-bit x86)
architecture.

```
$ sudo ./install
[...]
Done! A Debian root file tree was created in /tmp/debian-install.abc/buster-i386
```

Note that the file tree is created in /tmp, which gets cleaned every
reboot on most distributions, so you'll want to move it somewhere else.

# Second stage:

If you want to be able to use apt-get etc., you'll need to finish the
debootstrap.

First, change into the directory the install command listed, then run
these commands: 

```
$ sudo mount -o bind /dev dev
$ sudo chroot . /bin/bash
# /debootstrap/debootstrap --second-stage
```

You might want to edit
[/etc/apt/sources.list](https://wiki.debian.org/SourcesList#Example_sources.list)
to enable the update or contrib and non-free repos.

You can then run apt-get update and have a full Debian chroot.

Note that for normal use you will probably have to bind-mount a few
more directories such as /proc and /dev/pts into the chroot.

# Fakeroot:

If you want to install without root privileges, and you have installed
fakeroot, then you can run the install script with `FAKEROOT=fakeroot
./install`.

For chrooting into the created filesystem, you can either use `sudo
fakeroot chroot . /bin/bash` or, with
[proot](https://proot-me.github.io/), `fakeroot proot -R .`.